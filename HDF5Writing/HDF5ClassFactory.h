#ifndef HDF5Writing_HDF5ClassFactory_H
#define HDF5Writing_HDF5ClassFactory_H
#include <H5Cpp.h>

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

namespace XAMPP {
    class HDF5DataSetBuffer;
    class HDF5GroupBuffer;

    class HDF5DataSetBuffer {
    public:
        HDF5DataSetBuffer(std::shared_ptr<H5::Group> DSGrp);
        virtual ~HDF5DataSetBuffer();

        // Naming functions
        void setDSName(const std::string& ds_name);
        std::string name() const;

        /// Check if the Buffer has already been initialized
        bool isInitialized() const;

        /// Initalize the buffer. From this point no changes possible
        bool init();
        /// Write the last missing events to the buffer as well as
        /// meta information
        bool finalize();

        /// Adda a variable to the buffer with an optional label
        bool addVariable(const std::string& var_name, const std::string& label = "");
        /// Get the list of variables
        const std::vector<std::string>& variables() const;
        /// Get the list of labels
        const std::vector<std::string>& labels() const;
        /// Get the position of the corresponding variable in the vector
        unsigned int getVariableId(const std::string& var_name) const;

        void setDataType(H5::PredType type);

        /// Fill a variable identified by its name
        bool fillVariable(const std::string& var_name, double value);
        /// Fill a variable identified by its id which is its corresponding
        /// position in the variables() vector
        bool fillVariable(unsigned int var_id, double value);

        /// Pipe the full list of samples to the method
        bool fillVariable(const std::vector<double>& values);

        /// Get the value of the i-th variable in the current sample
        double getValue(unsigned int var_id) const;

        /// Write everying to disk. getValue will assign non-sense then
        bool fillEvent();

        /// Activate whether the moments, mean, sigma and covariance matric in
        /// the attributes of the datasets
        void saveMoments(bool B = true);

        /// Fill the moments from the last events
        void fillMoments(const double W);

        unsigned int size() const;         // How many variables are stored at the moment
        unsigned int buffer_size() const;  // How big is the buffer before the dataset is written to the I/O
        void set_buffer_size(unsigned int n);

        /// How many events have been stored in the buffer
        long unsigned int events() const;

    protected:
        std::shared_ptr<H5::DataSet> H5dataSet() const;

    private:
        // Delete the copy constructors
        HDF5DataSetBuffer(const HDF5DataSetBuffer&) = delete;
        void operator=(const HDF5DataSetBuffer&) = delete;

        double getValue(unsigned int x, unsigned int y) const;
        bool extendDataSet(unsigned int n_evts);
        bool saveAttribute(std::unique_ptr<float[]>& attribute, const std::string& name, unsigned int dim = 1);

        bool saveAttribute(const std::vector<std::string>& attribute, const std::string& attr_name);

        // Pointers to the H5 File objects
        std::shared_ptr<H5::Group> m_outGroup;
        std::shared_ptr<H5::DataSet> m_DS;

        // Size of the chunk per each dataset
        std::string m_name;
        // Buffering
        unsigned int m_buffer_size;
        unsigned int m_current_buffer_ptr;
        // Fill cycles
        unsigned int m_num_cycles;
        /// Variable names
        std::vector<std::string> m_variables;
        /// Variable labels
        std::vector<std::string> m_var_labels;
        unsigned int m_n_vars;

        std::unique_ptr<float[]> m_buffer;
        std::unique_ptr<float[]> m_max_buffer;
        std::unique_ptr<float[]> m_min_buffer;

        bool m_saveMoments;

        // Moments using raw data only
        std::unique_ptr<float[]> m_mean_buffer;
        std::unique_ptr<float[]> m_sigma_buffer;
        // Save the mean value considering the
        // individual event weights
        std::unique_ptr<float[]> m_w_mean_buffer;
        std::unique_ptr<float[]> m_w_sigma_buffer;
        // Save the covariance matrix
        std::unique_ptr<float[]> m_cov_buffer;
        std::unique_ptr<float[]> m_w_cov_buffer;

        double m_SumW;
        H5::PredType m_data_type;
    };

    class HDF5GroupBuffer {
    public:
        HDF5GroupBuffer(std::shared_ptr<H5::Group> DSGrp);
        HDF5GroupBuffer(std::shared_ptr<H5::H5File> H5File);
        virtual ~HDF5GroupBuffer();
        bool init();
        bool finalize();

        bool addEvent(const std::vector<double>& Variables, const double weight, const unsigned int classId);
        bool addVariable(const std::string& variable, const std::string& label);

        bool setGroup(const std::string& GroupName);

        std::string name() const;
        bool isInitialized() const;

        void set_buffer_size(unsigned int n);

        /// How many variables are stored
        unsigned int size() const;
        ///  How many events are stored
        long unsigned int events() const;

    protected:
        virtual std::shared_ptr<HDF5DataSetBuffer> createLabelingBuffer(std::shared_ptr<H5::Group> grp);
        std::shared_ptr<HDF5DataSetBuffer> data_buffer() const;
        std::shared_ptr<HDF5DataSetBuffer> weight_buffer() const;

    private:
        HDF5GroupBuffer();
        // Split the weights and  classifier from the data itself!
        std::string m_ClassName;
        std::shared_ptr<HDF5DataSetBuffer> m_Weights;
        std::shared_ptr<HDF5DataSetBuffer> m_Classifier;
        std::shared_ptr<HDF5DataSetBuffer> m_Data;

        std::shared_ptr<H5::H5File> m_outFile;
        std::shared_ptr<H5::Group> m_outGroup;
        std::mutex m_fill_mutex;
    };
    class HDF5RegressionBuffer : public HDF5GroupBuffer {
    public:
        HDF5RegressionBuffer(std::shared_ptr<H5::Group> DSGrp);
        HDF5RegressionBuffer(std::shared_ptr<H5::H5File> H5File);
        bool addTargetVariable(const std::string& variable, const std::string& label);

        bool addEvent(const std::vector<double>& Variables, const std::vector<double>& target, const double weight);

    protected:
        std::shared_ptr<HDF5DataSetBuffer> createLabelingBuffer(std::shared_ptr<H5::Group> grp) override;

    private:
        std::shared_ptr<HDF5DataSetBuffer> m_target;
        std::mutex m_fill_mutex;
    };
    class HDF5ClassFactory {
    public:
        virtual ~HDF5ClassFactory();
        HDF5ClassFactory();

        bool AddBackgroundTrainingEvent(const std::vector<double>& Variables, const double weight);
        bool AddBackgroundTestingEvent(const std::vector<double>& Variables, const double weight);

        bool AddSignalTrainingEvent(const std::vector<double>& Variables, const double weight);
        bool AddSignalTestingEvent(const std::vector<double>& Variables, const double weight);

        bool AddTrainingEvent(const std::vector<double>& Variables, const double weight, unsigned int class_id);
        bool AddTestingEvent(const std::vector<double>& Variables, const double weight, unsigned int class_id);

        bool isInitialized() const;
        void setDirectory(const std::string& directory);
        void setFileName(const std::string& location);

        bool addVariable(const std::string& var_name, const std::string& var_label = "");

        std::string outFile() const;
        std::string outDir() const;
        void close();

        unsigned int buffer_size() const;
        void set_buffer_size(unsigned int n);

        /// Add some string attribute to the dataset but only strings with less than 256 chars
        bool add_attribute(const std::string& attr_name, const std::string& value);
        /// Add an integer attribute to the dataset
        bool add_attribute(const std::string& attr_name, unsigned int value);

        /// How many variables are stored
        unsigned int size() const;
        ///  How many events are stored
        long unsigned int events() const;

    private:
        bool AddEvent(std::shared_ptr<HDF5GroupBuffer>& Buffer, const std::vector<double>& Variables, const double weight,
                      const unsigned int class_id);
        void finalizeBuffer(std::shared_ptr<HDF5GroupBuffer>& buf);

    protected:
        virtual bool initializeFile();
        virtual std::shared_ptr<HDF5GroupBuffer> createHDF5DataSet(const std::string& groupName);
        std::shared_ptr<HDF5GroupBuffer> trainingDS() const;
        std::shared_ptr<HDF5GroupBuffer> testingDS() const;
        std::shared_ptr<H5::H5File> H5File() const;

    private:
        // Boolean if the variable is initialized
        bool m_init;

    protected:
        std::vector<std::string> m_var_names;
        std::vector<std::string> m_var_labels;

    private:
        // Path of the H5 file
        std::string m_out_dir;
        std::string m_out_path;

        // The HDF5 file-pointer object
        std::shared_ptr<H5::H5File> m_outH5File;
        //
        std::shared_ptr<HDF5GroupBuffer> m_TrainingDS;
        std::shared_ptr<HDF5GroupBuffer> m_TestingDS;

        unsigned int m_bufferSize;
        std::mutex m_init_mutex;
    };

    class HDF5RegressionFactory : virtual public HDF5ClassFactory {
    public:
        HDF5RegressionFactory();
        bool addTargetVariable(const std::string& var_name, const std::string& var_label = "");

        bool AddTrainingEvent(const std::vector<double>& Variables, const std::vector<double>& target, const double weight);
        bool AddTestingEvent(const std::vector<double>& Variables, const std::vector<double>& target, const double weight);

    protected:
        std::shared_ptr<HDF5GroupBuffer> createHDF5DataSet(const std::string& groupName) override;
        bool initializeFile() override;

    private:
        bool AddEvent(std::shared_ptr<HDF5RegressionBuffer>& DS, const std::vector<double>& Variables, const std::vector<double>& target,
                      const double weight);

        std::vector<std::string> m_target_var_names;
        std::vector<std::string> m_target_var_labels;
        std::shared_ptr<HDF5RegressionBuffer> m_TrainDS;
        std::shared_ptr<HDF5RegressionBuffer> m_TestDS;
    };
}  // namespace XAMPP
#include <HDF5Writing/HDF5ClassFactory.ixx>
#endif
