#ifndef HDF5Writing_Utils_H
#define HDF5Writing_Utils_H

#include <TString.h>

#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>
namespace XAMPP {
    /// Returns if the element is already part of a vector
    template <class T, class P> bool IsElementInList(const std::vector<T>& vec, const P& elem) {
        for (auto& in_vec : vec) {
            if (in_vec == elem) return true;
        }
        return false;
    }
    /// Returns false if the given number is nan
    template <class T> bool IsFinite(const T& N) { return !(std::isnan(N) || std::isinf(N)); }

    bool DoesDirectoryExist(const std::string& Path);
    std::string WhiteSpaces(int N = 0, std::string space = " ");
    constexpr unsigned int MsgMethodChars = 60;
    template <class... Args> void Info(const std::string& method, const std::string& info_str, Args&&... args) {
        unsigned int l = method.size();
        std::cout << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
                  << " -- INFO: " << Form(info_str.c_str(), args...) << std::endl;
    }
    template <class... Args> void Warning(const std::string& method, const std::string& info_str, Args&&... args) {
        unsigned int l = method.size();
        std::cout << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
                  << " -- WARNING: " << Form(info_str.c_str(), args...) << std::endl;
    }
    template <class... Args> void Error(const std::string& method, const std::string& info_str, Args&&... args) {
        unsigned int l = method.size();
        std::cerr << method.substr(0, l < MsgMethodChars ? l : MsgMethodChars) << WhiteSpaces(MsgMethodChars - l)
                  << " -- ERROR: " << Form(info_str.c_str(), args...) << std::endl;
    }

}  // namespace XAMPP
#endif
