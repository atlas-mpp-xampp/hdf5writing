#
# File describing how to build externals needed by the analysis.
#


# Temporary directory for the build results:
set( _HDF5BuildDir "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/HDF5Build" )

# Set up the variables that the users can pick up HDF5 with:
set( HDF5_INCLUDE_DIRS
   $<BUILD_INTERFACE:${_HDF5BuildDir}/include>
   $<INSTALL_INTERFACE:include>
   ${ROOT_INCLUDE_DIRS} )


set( HDF5_LIBRARIES )
foreach( lib hdf5-shared hdf5_cpp-shared hdf5_hl-shared hdf5_tools-shared )
   list( APPEND HDF5_LIBRARIES
      $<BUILD_INTERFACE:${_HDF5BuildDir}/lib/${CMAKE_SHARED_LIBRARY_PREFIX}${lib}${CMAKE_SHARED_LIBRARY_SUFFIX}>
      $<INSTALL_INTERFACE:lib/${CMAKE_SHARED_LIBRARY_PREFIX}${lib}${CMAKE_SHARED_LIBRARY_SUFFIX}> )
endforeach()
 
find_package( ZLIB REQUIRED )


ExternalProject_Add( HDF5
   PREFIX ${CMAKE_BINARY_DIR}
   URL https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.1/src/hdf5-1.10.1.tar.gz
   URL_MD5 43a2f9466702fb1db31df98ae6677f15
   DOWNLOAD_NO_PROGRESS 1
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_HDF5BuildDir}
   -DHDF5_BUILD_CPP_LIB:BOOL=ON
   -DBUILD_SHARED_LIBS:BOOL=ON
   -DBUILD_STATIC_LIBS:BOOL=ON
   -DHDF5_BUILD_TOOLS:BOOL=ON
   -DZLIB_LIBRARY:FILEPATH=${ZLIB_LIBRARIES}
   -DZLIB_INCLUDE_DIR:PATH=${ZLIB_INCLUDE_DIRS}
   LOG_CONFIGURE 1 )

ExternalProject_Add_Step( HDF5 buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_HDF5BuildDir}/ <INSTALL_DIR>
   COMMENT "Installing HDF5 into the build area"
   DEPENDEES install )

ExternalProject_Add_Step( HDF5 linkshared
   COMMAND ln -f -s ${_HDF5BuildDir}/lib/libhdf5_cpp-shared.so.100.1.0  ${_HDF5BuildDir}/lib/libhdf5.so 
   COMMENT "Create shared symlink"
   DEPENDEES buildinstall )

ExternalProject_Add_Step( HDF5 linkshared_hl
   COMMAND ln -f -s ${_HDF5BuildDir}/lib/libhdf5_hl-shared.so.1.10.1  ${_HDF5BuildDir}/lib/libhdf5_hl.so 
   COMMENT "Create shared symlink"
   DEPENDEES buildinstall )

#######
# Install HDF5:
#######
install( DIRECTORY ${_HDF5BuildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS )
# Clean up:
unset( _HDF5BuildDir )


