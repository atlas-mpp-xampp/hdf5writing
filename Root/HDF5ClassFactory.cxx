#include <HDF5Writing/HDF5ClassFactory.h>
#include <HDF5Writing/Utils.h>
#include <TString.h>
#include <TSystem.h>

#include <cfloat>
namespace XAMPP {

    //########################################################################
    //                      HDF5DataSetBuffer
    //########################################################################
    HDF5DataSetBuffer::HDF5DataSetBuffer(std::shared_ptr<H5::Group> DSGrp) :
        m_outGroup(DSGrp),
        m_DS(),
        m_name(),
        m_buffer_size(100),
        m_current_buffer_ptr(0),
        m_num_cycles(0),
        m_variables(),
        m_var_labels(),
        m_n_vars(0),
        m_buffer(nullptr),
        m_max_buffer(nullptr),
        m_min_buffer(nullptr),
        m_saveMoments(false),
        m_mean_buffer(nullptr),
        m_sigma_buffer(nullptr),
        m_w_mean_buffer(nullptr),
        m_w_sigma_buffer(nullptr),
        m_cov_buffer(nullptr),
        m_w_cov_buffer(nullptr),
        m_SumW(0),
        m_data_type(H5::PredType::NATIVE_FLOAT) {}
    std::shared_ptr<H5::DataSet> HDF5DataSetBuffer::H5dataSet() const { return m_DS; }
    const std::vector<std::string>& HDF5DataSetBuffer::labels() const { return m_var_labels; }
    long unsigned int HDF5DataSetBuffer::events() const { return m_num_cycles * buffer_size() + m_current_buffer_ptr; }
    bool HDF5DataSetBuffer::finalize() {
        bool succeed = !isInitialized();

        long unsigned int max_events = events();
        if (isInitialized()) {
            if (!saveAttribute(m_max_buffer, "Maximum")) succeed = false;
            if (!saveAttribute(m_min_buffer, "Minimum")) succeed = false;
            if (m_saveMoments) {
                for (unsigned int i = 0; m_SumW > 0 && i < size(); ++i) {
                    m_w_mean_buffer[i] /= m_SumW;
                    m_w_sigma_buffer[i] = std::sqrt(m_w_sigma_buffer[i] / m_SumW - std::pow(m_w_mean_buffer[i], 2));
                }
                for (unsigned int i = 0; max_events > 0 && i < size(); ++i) {
                    m_mean_buffer[i] /= max_events;
                    m_sigma_buffer[i] = std::sqrt(m_sigma_buffer[i] / max_events - std::pow(m_mean_buffer[i], 2));
                }
                if (size() > 1) {
                    for (unsigned int i = 0; max_events > 0 && i < size(); ++i) {
                        for (unsigned int j = 0; j <= i; ++j) {
                            m_cov_buffer[i * size() + j] = m_cov_buffer[i * size() + j] / max_events - m_mean_buffer[i] * m_mean_buffer[j];
                            m_cov_buffer[j * size() + i] = m_cov_buffer[i * size() + j];
                        }
                    }
                    for (unsigned int i = 0; m_SumW > 0 && i < size(); ++i) {
                        for (unsigned int j = 0; j <= i; ++j) {
                            m_w_cov_buffer[i * size() + j] =
                                m_w_cov_buffer[i * size() + j] / m_SumW - m_w_mean_buffer[i] * m_w_mean_buffer[j];
                            m_w_cov_buffer[j * size() + i] = m_w_cov_buffer[i * size() + j];
                        }
                    }
                }
                if (!saveAttribute(m_mean_buffer, "Mean")) succeed = false;
                if (!saveAttribute(m_sigma_buffer, "Sigma")) succeed = false;
                if (!saveAttribute(m_w_mean_buffer, "WeightedMean")) succeed = false;
                if (!saveAttribute(m_w_sigma_buffer, "WeightedSigma")) succeed = false;
                if (size() > 1 && !saveAttribute(m_cov_buffer, "Covariance", 2)) succeed = false;
                if (size() > 1 && !saveAttribute(m_w_cov_buffer, "WeightedCovariance", 2)) succeed = false;
            }
            if (m_current_buffer_ptr > 0) {
                while (m_current_buffer_ptr != buffer_size()) {
                    for (unsigned int i = 0; i < size(); ++i) fillVariable(i, 0.);
                    ++m_current_buffer_ptr;
                }
                succeed = extendDataSet(m_current_buffer_ptr);
            }
            // Delete the buffer
            m_buffer.reset();
            // Write how many events have been written as attribute
            // Create the data space for the attribute.
            H5::DataSpace attr_dataspace(H5S_SCALAR);
            // Create a dataset attribute.
            try {
                H5::Attribute attribute = m_DS->createAttribute("nEvents", H5::PredType::STD_I32BE, attr_dataspace);
                // Write the attribute data.
                attribute.write(H5::PredType::NATIVE_INT, &max_events);
            } catch (...) { return false; }
        }
        return succeed;
    }
    bool HDF5DataSetBuffer::saveAttribute(std::unique_ptr<float[]>& data, const std::string& name, unsigned int dim) {
        hsize_t attr_dim[] = {size(), size()};
        H5::DataSpace attr_dataspace(dim, attr_dim, nullptr);
        // Create new string datatype for attribute
        try {
            H5::Attribute attribute = m_DS->createAttribute(name, m_data_type, attr_dataspace);
            attr_dataspace.close();
            attribute.write(m_data_type, data.get());
            attribute.close();
        } catch (...) { return false; }
        return true;
    }

    HDF5DataSetBuffer::~HDF5DataSetBuffer() {
        // Write the missing events to the output-file
        finalize();
    }

    bool HDF5DataSetBuffer::isInitialized() const { return m_buffer != nullptr; }

    void HDF5DataSetBuffer::setDSName(const std::string& ds_name) {
        if (isInitialized() || ds_name.empty()) {
            Warning("HDF5DataSetBuffer()", "Could not set the dataset name to %s", ds_name.c_str());
            return;
        }
        m_name = ds_name;
    }
    std::string HDF5DataSetBuffer::name() const { return m_name; }

    bool HDF5DataSetBuffer::addVariable(const std::string& var_name, const std::string& var_label) {
        if (isInitialized()) {
            Error("HDF5DataSetBuffer()", "The layout of the dataset %s is already frozen", name().c_str());
            return false;
        } else if (var_name.empty()) {
            Error("HDF5DataSetBuffer()", "Empty variable names are not allowed");
            return false;
        } else if (getVariableId(var_name) != size()) {
            Error("HDF5DataSetBuffer()", "The variable %s has already been defined.", var_name.c_str());
            return false;
        }
        m_variables.push_back(var_name);
        m_var_labels.push_back(var_label);
        m_n_vars = variables().size();
        return true;
    }
    unsigned int HDF5DataSetBuffer::getVariableId(const std::string& var_name) const {
        unsigned int ID = 0;
        for (const auto& existent_var : variables()) {
            if (existent_var == var_name) return ID;
            ++ID;
        }
        return size();
    }
    bool HDF5DataSetBuffer::fillVariable(const std::string& var_name, double value) { return fillVariable(getVariableId(var_name), value); }
    bool HDF5DataSetBuffer::fillVariable(unsigned int var_id, double value) {
        if (!isInitialized()) {
            Error("HDF5DataSetBuffer()", "The dataset %s has not been initialized yet.", name().c_str());
            return false;
        } else if (var_id >= size()) {
            Error("HDF5DataSetBuffer()", "Invalid variable identifier %u", var_id);
            return false;
        } else if (!IsFinite(value)) {
            Error("HDF5DataSetBuffer()", "The given value is not a number");
            return false;
        }
        // Column -> var_id  || row current_buffer_ptr
        int i = size() * m_current_buffer_ptr + var_id;
        m_buffer[i] = value;
        // Cache the maximum and minimum of each variable
        if (m_max_buffer[var_id] < value) m_max_buffer[var_id] = value;
        if (m_min_buffer[var_id] > value) m_min_buffer[var_id] = value;
        return true;
    }
    bool HDF5DataSetBuffer::fillVariable(const std::vector<double>& values) {
        if (values.size() != size()) {
            Error("HDF5DataSetBuffer()", "Insufficient amount of data given");
            return false;
        }
        unsigned int N = 0;
        for (const auto& V : values) {
            if (!fillVariable(N, V)) return false;
            ++N;
        }
        return true;
    }
    double HDF5DataSetBuffer::getValue(unsigned int column, unsigned int row) const {
        if (column >= size()) {
            Error("HDF5DataSetBuffer()", "Invalid x-address: %u. Allowed: %u", column, size());
        } else if (row >= buffer_size()) {
            Error("HDF5DataSetBuffer()", "Invalid y-address: %u. Allowed: %u", row, buffer_size());
        } else {
            unsigned int i = row * size() + column;
            return m_buffer[i];
        }
        return std::nan("nan");
    }
    double HDF5DataSetBuffer::getValue(unsigned int var_id) const { return getValue(var_id, m_current_buffer_ptr); }
    bool HDF5DataSetBuffer::extendDataSet(unsigned int n_evts) {
        // Extend the dataset -> it becomes (cycles) x (number of variables)
        hsize_t extension[2];
        extension[0] = (m_num_cycles + 1) * buffer_size();
        extension[1] = size();
        H5dataSet()->extend(extension);
        hsize_t offset[2];
        offset[0] = m_num_cycles * buffer_size();
        offset[1] = 0;
        hsize_t dims[2];
        dims[0] = n_evts;
        dims[1] = size();

        std::unique_ptr<H5::DataSpace> filespace = std::make_unique<H5::DataSpace>(H5dataSet()->getSpace());
        filespace->selectHyperslab(H5S_SELECT_SET, dims, offset);
        std::unique_ptr<H5::DataSpace> memspace = std::make_unique<H5::DataSpace>(size() > 1 ? 2 : 1, dims, nullptr);
        H5dataSet()->write(m_buffer.get(), m_data_type, *memspace, *filespace);
        return true;
    }

    bool HDF5DataSetBuffer::fillEvent() {
        if (!isInitialized()) {
            Error("HDF5DataSetBuffer()", "The dataset %s has not been initialized yet.", name().c_str());
            return false;
        }
        ++m_current_buffer_ptr;
        // The chunk has been filled entirely -> write to I/O
        if (m_current_buffer_ptr == buffer_size()) {
            m_current_buffer_ptr = 0;
            if (m_num_cycles == 0) {
                H5dataSet()->write(m_buffer.get(), m_data_type);
            } else if (!extendDataSet(buffer_size())) {
                return false;
            }
            ++m_num_cycles;
        }
        return true;
    }
    unsigned int HDF5DataSetBuffer::size() const { return m_n_vars; }
    unsigned int HDF5DataSetBuffer::buffer_size() const { return m_buffer_size; }
    void HDF5DataSetBuffer::set_buffer_size(unsigned int n) {
        if (!isInitialized() && n > 0)
            m_buffer_size = n;
        else {
            Warning("HDF5DataSetBuffer()", "Buffer size could not be set");
        }
    }
    void HDF5DataSetBuffer::setDataType(H5::PredType type) {
        if (!isInitialized()) m_data_type = type;
    }
    bool HDF5DataSetBuffer::init() {
        if (isInitialized()) {
            Warning("HDF5DataSetBuffer()", "Is already initialized");
            return true;
        }
        if (!size()) {
            Error("HDF5DataSetBuffer()", "No variables have been defined");
            return false;
        } else if (!buffer_size()) {
            Error("HDF5DataSetBuffer()", "No buffer has been defined");
            return false;
        }
        if (name().empty()) {
            if (size() == 1) {
                m_name = (*m_variables.begin());
                Info("HDF5DataSetBuffer()", "Set name to %s.", name().c_str());
            } else {
                Error("HDF5DataSetBuffer()", "Dataset has no name");
                return false;
            }
        }
        // fix the size of the pointer etc
        m_current_buffer_ptr = 0;

        // Initializing the buffer
        // Initializing a 2D array does not work since the space in memory is not continueously allocated
        // Let's try this way
        m_buffer = std::unique_ptr<float[]>(new float[buffer_size() * size()]);
        m_max_buffer = std::unique_ptr<float[]>(new float[size()]);
        m_min_buffer = std::unique_ptr<float[]>(new float[size()]);
        if (m_saveMoments) {
            m_mean_buffer = std::unique_ptr<float[]>(new float[size()]);
            m_sigma_buffer = std::unique_ptr<float[]>(new float[size()]);
            m_w_mean_buffer = std::unique_ptr<float[]>(new float[size()]);
            m_w_sigma_buffer = std::unique_ptr<float[]>(new float[size()]);
            if (size() > 1) {
                m_cov_buffer = std::unique_ptr<float[]>(new float[size() * size()]);
                m_w_cov_buffer = std::unique_ptr<float[]>(new float[size() * size()]);
            }
        }
        for (unsigned int i = 0; i < buffer_size() * size(); ++i) { m_buffer[i] = 0.; }
        for (unsigned int j = 0; j < size(); ++j) {
            m_max_buffer[j] = FLT_MIN;
            m_min_buffer[j] = FLT_MAX;
            if (m_saveMoments) {
                m_mean_buffer[j] = 0;
                m_sigma_buffer[j] = 0;
                m_w_mean_buffer[j] = 0;
                m_w_sigma_buffer[j] = 0;
                if (size() > 1) {
                    for (unsigned int k = 0; k < size(); ++k) {
                        m_cov_buffer[j * size() + k] = 0.;
                        m_w_cov_buffer[j * size() + k] = 0.;
                    }
                }
            }
        }
        // Creating a unlimited dataset towards the number of events the other dimension is the number of variables
        hsize_t maxdims[2];
        maxdims[0] = H5S_UNLIMITED;
        maxdims[1] = size();

        // Let's tell H5 about the buffer size
        hsize_t dims[2];
        dims[0] = buffer_size();
        dims[1] = size();

        std::unique_ptr<H5::DataSpace> dataspace = std::make_unique<H5::DataSpace>(size() > 1 ? 2 : 1, dims, maxdims);

        // Modify dataset creation property to enable chunking
        hsize_t chunk_dims[2];
        chunk_dims[0] = buffer_size();
        chunk_dims[1] = size();

        H5::DSetCreatPropList prop;
        prop.setChunk(size() > 1 ? 2 : 1, chunk_dims);
        if (m_outGroup)
            m_DS = std::make_shared<H5::DataSet>(m_outGroup->createDataSet(name().c_str(), m_data_type, *dataspace, prop));
        else
            return false;

        if (!saveAttribute(m_variables, "variables")) return false;
        if (!saveAttribute(m_var_labels, "labels")) return false;
        return true;
    }

    bool HDF5DataSetBuffer::saveAttribute(const std::vector<std::string>& attribute, const std::string& attr_name) {
        if (attribute.empty()) {
            Error("HDF5DataSetBuffer()", "Empty attributes given for %s", attr_name.c_str());
            return false;
        }
        unsigned int max_letters = 0;
        for (const auto& str : attribute) { max_letters = max_letters < str.size() ? str.size() : max_letters; }

        max_letters += 1;  // Increment by one unit to include the final char of the string
        std::unique_ptr<char[]> name_attr(new char[size() * max_letters]);
        memset((void*)name_attr.get(), 0, size() * max_letters);
        int i = 0;
        for (auto& var : m_variables) {
            var.copy(name_attr.get() + (i * max_letters), max_letters - 1);
            ++i;
        }
        hsize_t attr_dim[] = {size()};
        H5::DataSpace attr_dataspace(1, attr_dim, nullptr);
        // Create new string datatype for attribute
        try {
            H5::StrType strdatatype(H5::PredType::C_S1, max_letters);  // of length 256 characters
            H5::Attribute attribute = m_DS->createAttribute(attr_name, strdatatype, attr_dataspace);

            attr_dataspace.close();
            attribute.write(strdatatype, name_attr.get());
            attribute.close();
        } catch (...) {
            Error("HDFDataSetBuffer()", "Attribute writing of %s failed", attr_name.c_str());
            return false;
        }
        return true;
    }
    const std::vector<std::string>& HDF5DataSetBuffer::variables() const { return m_variables; }
    void HDF5DataSetBuffer::saveMoments(bool B) { m_saveMoments = B; }
    // Fill the moments using the weight
    void HDF5DataSetBuffer::fillMoments(const double W) {
        if (!m_saveMoments) return;
        m_SumW += W;
        for (unsigned int i = 0; i < size(); ++i) {
            m_mean_buffer[i] += getValue(i);
            m_w_mean_buffer[i] += getValue(i) * W;
            double sigma = std::pow(getValue(i), 2);
            m_sigma_buffer[i] += sigma;
            m_w_sigma_buffer[i] += sigma * W;
            for (unsigned int j = 0; j <= i; ++j) {
                double cov = getValue(i) * getValue(j);
                m_cov_buffer[i * size() + j] += cov;
                m_w_cov_buffer[i * size() + j] += cov * W;
            }
        }
    }

    //########################################################################
    //                      HDF5GroupBuffer
    //########################################################################
    HDF5GroupBuffer::HDF5GroupBuffer() : m_ClassName(), m_Weights(), m_Classifier(), m_Data(), m_outFile(), m_outGroup(), m_fill_mutex() {
        // Save temporarilly one instance of the Buffer in order not to define everything twice
        m_Data = std::make_shared<HDF5DataSetBuffer>(m_outFile);
    }
    unsigned int HDF5GroupBuffer::size() const { return m_Data->size(); }
    long unsigned int HDF5GroupBuffer::events() const { return m_Data->events(); }
    void HDF5GroupBuffer::set_buffer_size(unsigned int n) { m_Data->set_buffer_size(n); }
    HDF5GroupBuffer::HDF5GroupBuffer(std::shared_ptr<H5::Group> DSGrp) : HDF5GroupBuffer() { m_outGroup = DSGrp; }
    HDF5GroupBuffer::HDF5GroupBuffer(std::shared_ptr<H5::H5File> H5File) : HDF5GroupBuffer() { m_outFile = H5File; }
    HDF5GroupBuffer::~HDF5GroupBuffer() {}
    bool HDF5GroupBuffer::setGroup(const std::string& GrpName) {
        if (isInitialized()) {
            Error("HDF5DataSetBuffer()", "The layout of the dataset %s is already frozen", name().c_str());
            return false;
        } else if (GrpName.empty()) {
            Error("HDF5DataSetBuffer()", "Empty group names are not allowed");
            return false;
        }
        try {
            if (!m_outGroup && m_outFile)
                m_outGroup = std::make_shared<H5::Group>(m_outFile->createGroup(GrpName.c_str()));
            else if (m_outGroup)
                m_outGroup = std::make_shared<H5::Group>(m_outGroup->createGroup(GrpName.c_str()));
            else {
                Error("HDF5DataSetBuffer()", "Could not create group %s.", GrpName.c_str());
                return false;
            }
            Info("HDF5DataSetBuffer()", "New group element %s has been created", GrpName.c_str());
        } catch (const H5::GroupIException& error) {
            error.printErrorStack();
            return false;
        } catch (const H5::FileIException& error) {
            error.printErrorStack();
            return false;
        }
        m_ClassName = GrpName;
        return true;
    }
    std::string HDF5GroupBuffer::name() const { return m_ClassName; }
    bool HDF5GroupBuffer::isInitialized() const { return m_Classifier != nullptr; }
    std::shared_ptr<HDF5DataSetBuffer> HDF5GroupBuffer::createLabelingBuffer(std::shared_ptr<H5::Group> grp) {
        std::shared_ptr<HDF5DataSetBuffer> classifier = std::make_shared<HDF5DataSetBuffer>(grp);
        classifier->addVariable("ClassID");
        // classifier->setDataType(H5::PredType::NATIVE_INT);
        return classifier;
    }
    bool HDF5GroupBuffer::init() {
        if (isInitialized()) { return true; }
        // Create the classification dataset
        if (!m_outFile && !m_outGroup) {
            Error("sciKitClassificationBuffer()", "Neither groups or outfiles were given");
            return false;
        }
        // First cache the current variable names before deleting the old instance of the buffer
        std::vector<std::string> var_names = m_Data->variables();
        unsigned int buffer_size = m_Data->buffer_size();
        if (m_outGroup) {
            m_Classifier = createLabelingBuffer(m_outGroup);
            m_Data = std::make_shared<HDF5DataSetBuffer>(m_outGroup);
            m_Weights = std::make_shared<HDF5DataSetBuffer>(m_outGroup);
        }
        // Just save them in the file
        else {
            m_Classifier = createLabelingBuffer(m_outFile);
            m_Data = std::make_shared<HDF5DataSetBuffer>(m_outFile);
            m_Weights = std::make_shared<HDF5DataSetBuffer>(m_outFile);
        }
        // Set the size of the buffers / chunks
        m_Classifier->set_buffer_size(buffer_size);
        m_Data->set_buffer_size(buffer_size);
        m_Weights->set_buffer_size(buffer_size);

        m_Weights->addVariable("Weights");
        // m_Weights->setDataType(H5::PredType::NATIVE_DOUBLE);
        if (!m_Weights->init()) {
            Error("HDF5GroupBuffer()", "Failed to initialize the weights");
            return false;
        }
        m_Classifier->setDSName("ClassID");
        if (!m_Classifier->init()) {
            Error("HDF5GroupBuffer()", "Failed to initialize the dataClassification");
            return false;
        }
        m_Data->setDataType(H5::PredType::NATIVE_FLOAT);
        m_Data->setDSName("Data");
        m_Data->saveMoments(true);
        for (auto& V : var_names) m_Data->addVariable(V);

        return m_Data->init();
    }
    bool HDF5GroupBuffer::finalize() {
        if (isInitialized()) {
            if (!m_Data->finalize() || !m_Weights->finalize() || !m_Classifier->finalize()) return false;
        }
        return true;
    }
    bool HDF5GroupBuffer::addVariable(const std::string& variable, const std::string& label) {
        return m_Data->addVariable(variable, label);
    }
    bool HDF5GroupBuffer::addEvent(const std::vector<double>& Variables, const double weight, const unsigned int classId) {
        std::lock_guard<std::mutex> guard(m_fill_mutex);
        if (!init()) return false;
        if (!m_Data->fillVariable(Variables)) return false;
        if (!m_Weights->fillVariable(0, weight)) return false;
        if (!m_Classifier->fillVariable(0, classId)) return false;
        m_Data->fillMoments(weight);
        if (!m_Weights->fillEvent() || !m_Data->fillEvent() || !m_Classifier->fillEvent()) { return false; }
        return true;
    }
    std::shared_ptr<HDF5DataSetBuffer> HDF5GroupBuffer::data_buffer() const { return m_Data; }
    std::shared_ptr<HDF5DataSetBuffer> HDF5GroupBuffer::weight_buffer() const { return m_Weights; }

    //########################################################################
    //                      HDF5RegressionBuffer
    //########################################################################
    HDF5RegressionBuffer::HDF5RegressionBuffer(std::shared_ptr<H5::Group> DSGrp) :
        HDF5GroupBuffer(DSGrp), m_target(std::make_shared<HDF5DataSetBuffer>(DSGrp)), m_fill_mutex() {}
    HDF5RegressionBuffer::HDF5RegressionBuffer(std::shared_ptr<H5::H5File> H5File) :
        HDF5GroupBuffer(H5File), m_target(std::make_shared<HDF5DataSetBuffer>(H5File)) {}
    bool HDF5RegressionBuffer::addTargetVariable(const std::string& variable, const std::string& label) {
        return m_target->addVariable(variable, label);
    }
    std::shared_ptr<HDF5DataSetBuffer> HDF5RegressionBuffer::createLabelingBuffer(std::shared_ptr<H5::Group> grp) {
        std::shared_ptr<HDF5DataSetBuffer> buffer = std::make_shared<HDF5DataSetBuffer>(grp);
        buffer->saveMoments(true);
        for (const auto& V : m_target->variables()) buffer->addVariable(V);
        m_target = buffer;
        return buffer;
    }
    bool HDF5RegressionBuffer::addEvent(const std::vector<double>& Variables, const std::vector<double>& target, const double weight) {
        std::lock_guard<std::mutex> guard(m_fill_mutex);
        if (!init()) return false;
        if (!data_buffer()->fillVariable(Variables)) return false;
        if (!m_target->fillVariable(target)) return false;
        if (!weight_buffer()->fillVariable(0, weight)) return false;
        data_buffer()->fillMoments(weight);
        m_target->fillMoments(weight);
        if (!weight_buffer()->fillEvent() || !data_buffer()->fillEvent() || !m_target->fillEvent()) { return false; }
        return true;
    }

    //########################################################################
    //                      HDF5ClassFactory
    //########################################################################
    HDF5ClassFactory::HDF5ClassFactory() :
        m_init(false),
        m_var_names(),
        m_var_labels(),
        m_out_dir("SciKitLearn"),
        m_out_path("OutFile.HDF5"),
        m_outH5File(),
        m_TrainingDS(),
        m_TestingDS(),
        m_bufferSize(10000),
        m_init_mutex() {}
    unsigned int HDF5ClassFactory::buffer_size() const { return m_bufferSize; }
    void HDF5ClassFactory::set_buffer_size(unsigned int n) { m_bufferSize = n; }

    void HDF5ClassFactory::finalizeBuffer(std::shared_ptr<HDF5GroupBuffer>& buf) {
        if (buf) { buf->finalize(); }
        buf = std::shared_ptr<HDF5GroupBuffer>();
    }
    void HDF5ClassFactory::close() {
        finalizeBuffer(m_TrainingDS);
        finalizeBuffer(m_TestingDS);
        if (m_outH5File) {
            Info("HDF5ClassFactory()", "Close the H5 file");
            m_outH5File->close();
        }
        m_outH5File = std::shared_ptr<H5::H5File>();
    }
    HDF5ClassFactory::~HDF5ClassFactory() { close(); }
    bool HDF5ClassFactory::addVariable(const std::string& var_name, const std::string& var_label) {
        if (isInitialized()) {
            Error("HDF5ClassFactory()", "The factory is already initialized");
            return false;
        } else if (IsElementInList(m_var_names, var_name)) {
            Error("HDF5ClassFactory()", "The variable %s has already been added", var_name.c_str());
            return false;
        }
        m_var_names.push_back(var_name);
        m_var_labels.push_back(var_label);
        return true;
    }
    bool HDF5ClassFactory::isInitialized() const { return m_init; }
    bool HDF5ClassFactory::AddBackgroundTrainingEvent(const std::vector<double>& variables, const double weight) {
        return AddEvent(m_TrainingDS, variables, weight, false);
    }
    bool HDF5ClassFactory::AddBackgroundTestingEvent(const std::vector<double>& variables, const double weight) {
        return AddEvent(m_TestingDS, variables, weight, false);
    }
    bool HDF5ClassFactory::AddSignalTrainingEvent(const std::vector<double>& variables, const double weight) {
        return AddEvent(m_TrainingDS, variables, weight, true);
    }
    bool HDF5ClassFactory::AddSignalTestingEvent(const std::vector<double>& variables, const double weight) {
        return AddEvent(m_TestingDS, variables, weight, true);
    }
    bool HDF5ClassFactory::AddTrainingEvent(const std::vector<double>& variables, const double weight, unsigned int class_id) {
        return AddEvent(m_TrainingDS, variables, weight, class_id);
    }
    bool HDF5ClassFactory::AddTestingEvent(const std::vector<double>& variables, const double weight, unsigned int class_id) {
        return AddEvent(m_TestingDS, variables, weight, class_id);
    }
    bool HDF5ClassFactory::AddEvent(std::shared_ptr<HDF5GroupBuffer>& Buffer, const std::vector<double>& Variables, const double weight,
                                    const unsigned int class_id) {
        if (!initializeFile()) return false;
        return Buffer->addEvent(Variables, weight, class_id);
    }

    bool HDF5ClassFactory::initializeFile() {
        // The file has already been initialized
        if (isInitialized()) { return true; }
        /// Lock the method to make sure that it can only be initialized once
        std::lock_guard<std::mutex> guard(m_init_mutex);
        /// In cases of a second thread this will protect the init method
        if (isInitialized()) { return true; }

        // Chec if any variables have been added to the Factory
        if (m_var_names.empty()) {
            Error("HDF5ClassFactory()", "No variables have been defined in the factory.");
            return false;
        }
        // Check if the outfile name is not empty and just only a directory
        if (m_out_path.empty()) {
            Error("HDF5ClassFactory()", "No path has been given");
            return false;
        } else if (m_out_path.rfind("/") == m_out_path.size() - 1) {
            Error("HDF5ClassFactory()", "You only have given a directory: %s. What is the name of the file?", m_out_path.c_str());
            return false;
        }
        // Create the directory
        if (!m_out_dir.empty()) {
            if (m_out_dir.rfind("/") != m_out_dir.size() - 1) m_out_dir += "/";
            if (!DoesDirectoryExist(m_out_dir)) {
                Info("HDF5ClassFactory()", "Create directory %s", m_out_dir.c_str());
                gSystem->mkdir(m_out_dir.c_str(), true);
            }
        }
        Info("HDF5ClassFactory()", "Create new H5 File: %s%s", m_out_dir.c_str(), m_out_path.c_str());
        // Create the output File
        try {
            m_outH5File = std::make_shared<H5::H5File>((m_out_dir + m_out_path).c_str(), H5F_ACC_TRUNC);
        } catch (const H5::GroupIException& error) {
            error.printErrorStack();
            return false;
        }
        // Now create the GroupPointers for training and testing
        m_TrainingDS = createHDF5DataSet("Training/");
        m_TestingDS = createHDF5DataSet("Testing/");
        if (!m_TrainingDS || !m_TestingDS) return false;
        m_var_names.clear();
        m_var_labels.clear();

        m_init = true;
        return true;
    }
    bool HDF5ClassFactory::add_attribute(const std::string& attr_name, const std::string& value) {
        size_t label_size = value.size() + 1;
        std::unique_ptr<char[]> name_attr(new char[label_size]);
        memset((void*)name_attr.get(), 0, label_size);
        value.copy(name_attr.get(), value.size());
        try {
            H5::DataSpace attr_dataspace(H5S_SCALAR);
            H5::StrType strdatatype(H5::PredType::C_S1, label_size);  // of length 256 characters
            H5::Attribute attribute = m_outH5File->createAttribute(attr_name, strdatatype, attr_dataspace);

            attr_dataspace.close();
            attribute.write(strdatatype, name_attr.get());
            attribute.close();

        } catch (...) { return false; }
        return true;
    }

    bool HDF5ClassFactory::add_attribute(const std::string& attr_name, unsigned int value) {
        try {
            // Write how many events have been written as attribute
            // Create the data space for the attribute.
            H5::DataSpace attr_dataspace(H5S_SCALAR);
            // Create a dataset attribute.

            H5::Attribute attribute = m_outH5File->createAttribute(attr_name, H5::PredType::STD_I32BE, attr_dataspace);
            // Write the attribute data.
            attribute.write(H5::PredType::NATIVE_INT, &value);

        } catch (...) { return false; }
        return true;
    }
    std::shared_ptr<HDF5GroupBuffer> HDF5ClassFactory::createHDF5DataSet(const std::string& groupName) {
        std::shared_ptr<HDF5GroupBuffer> Ptr = std::make_shared<HDF5GroupBuffer>(m_outH5File);
        Ptr->setGroup(groupName);
        Ptr->set_buffer_size(buffer_size());
        // Adding the variables to the dataset
        for (size_t v = 0; v < m_var_names.size(); ++v) {
            if (!Ptr->addVariable(m_var_names[v], m_var_labels[v])) return std::shared_ptr<HDF5GroupBuffer>();
        }
        if (!Ptr->init()) return std::shared_ptr<HDF5GroupBuffer>();
        return Ptr;
    }
    void HDF5ClassFactory::setFileName(const std::string& location) {
        if (!isInitialized()) m_out_path = location;
    }
    void HDF5ClassFactory::setDirectory(const std::string& directory) {
        if (!isInitialized()) m_out_dir = directory;
    }
    std::string HDF5ClassFactory::outFile() const { return m_out_path; }
    std::string HDF5ClassFactory::outDir() const { return m_out_dir; }
    std::shared_ptr<HDF5GroupBuffer> HDF5ClassFactory::trainingDS() const { return m_TrainingDS; }
    std::shared_ptr<HDF5GroupBuffer> HDF5ClassFactory::testingDS() const { return m_TestingDS; }
    std::shared_ptr<H5::H5File> HDF5ClassFactory::H5File() const { return m_outH5File; }
    unsigned int HDF5ClassFactory::size() const { return isInitialized() ? m_TrainingDS->size() : m_var_names.size(); }
    long unsigned int HDF5ClassFactory::events() const { return isInitialized() ? m_TrainingDS->events() + m_TestingDS->events() : 0; }

    //########################################################################
    //                      HDF5RegressionFactory
    //########################################################################

    HDF5RegressionFactory::HDF5RegressionFactory() :
        HDF5ClassFactory(), m_target_var_names(), m_target_var_labels(), m_TrainDS(nullptr), m_TestDS(nullptr) {}
    bool HDF5RegressionFactory::addTargetVariable(const std::string& var_name, const std::string& target_label) {
        if (isInitialized()) {
            Error("HDF5ClassFactory()", "The factory is already initialized");
            return false;
        } else if (IsElementInList(m_target_var_names, var_name)) {
            Error("HDF5ClassFactory()", "The variable %s has already been added", var_name.c_str());
            return false;
        }
        m_target_var_names.push_back(var_name);
        m_target_var_labels.push_back(target_label);
        return true;
    }
    std::shared_ptr<HDF5GroupBuffer> HDF5RegressionFactory::createHDF5DataSet(const std::string& groupName) {
        std::shared_ptr<HDF5RegressionBuffer> reg_ptr = std::make_shared<HDF5RegressionBuffer>(H5File());
        reg_ptr->set_buffer_size(buffer_size());
        reg_ptr->setGroup(groupName);
        // Adding the variables to the dataset
        for (size_t t = 0; t < m_target_var_names.size(); ++t) {
            if (!reg_ptr->addTargetVariable(m_target_var_names[t], m_target_var_labels[t])) return std::shared_ptr<HDF5GroupBuffer>();
        }
        for (size_t v = 0; v < m_var_names.size(); ++v) {
            if (!reg_ptr->addVariable(m_var_names[v], m_var_labels[v])) return std::shared_ptr<HDF5GroupBuffer>();
        }
        if (!reg_ptr->init()) return std::shared_ptr<HDF5GroupBuffer>();
        return reg_ptr;
    }
    bool HDF5RegressionFactory::initializeFile() {
        if (!HDF5ClassFactory::initializeFile()) return false;
        m_TrainDS = std::dynamic_pointer_cast<HDF5RegressionBuffer>(trainingDS());
        m_TestDS = std::dynamic_pointer_cast<HDF5RegressionBuffer>(testingDS());
        m_target_var_names.clear();
        m_target_var_labels.clear();
        return m_TestDS != nullptr && m_TrainDS != nullptr;
    }
    bool HDF5RegressionFactory::AddTrainingEvent(const std::vector<double>& Variables, const std::vector<double>& target,
                                                 const double weight) {
        return AddEvent(m_TrainDS, Variables, target, weight);
    }
    bool HDF5RegressionFactory::AddTestingEvent(const std::vector<double>& Variables, const std::vector<double>& target,
                                                const double weight) {
        return AddEvent(m_TestDS, Variables, target, weight);
    }
    bool HDF5RegressionFactory::AddEvent(std::shared_ptr<HDF5RegressionBuffer>& DS, const std::vector<double>& Variables,
                                         const std::vector<double>& target, const double weight) {
        if (!initializeFile()) return false;
        return DS->addEvent(Variables, target, weight);
    }

}  // namespace XAMPP
