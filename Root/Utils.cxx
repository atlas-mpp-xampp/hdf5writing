#include <HDF5Writing/Utils.h>
#include <dirent.h>
namespace XAMPP {

    bool DoesDirectoryExist(const std::string &Path) {
        DIR *dir = opendir(Path.c_str());
        bool Exist = dir != nullptr;
        if (Exist) closedir(dir);
        return Exist;
    }
    std::string WhiteSpaces(int N, std::string space) {
        std::string White;
        for (int n = 0; n < N; ++n) White += space;
        return White;
    }

}  // namespace XAMPP
