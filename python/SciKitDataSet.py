#!/bin/env/python
from ClusterSubmission.Utils import ResolvePath
import os, threading, math, time
import numpy as np
import h5py
import logging


class SciKitDataSetMetaData(object):
    def __init__(self, data_group):
        self.__chunk_size = data_group.chunks[0]
        self.__varnames = self.__assign_attr(data_group, "variables")
        self.__labels = self.__assign_attr(data_group, "labels")
        for n in range(len(self.__labels)):
            if len(self.__labels[n]) == 0:
                self.__labels[n] = self.__varnames[n]

        self.__minima = self.__assign_attr(data_group, "Minimum")
        self.__maxima = self.__assign_attr(data_group, "Maximum")

        self.__mean = self.__assign_attr(data_group, "Mean")
        self.__w_mean = self.__assign_attr(data_group, "WeightedMean")

        self.__sigma = self.__assign_attr(data_group, "Sigma")
        self.__w_sigma = self.__assign_attr(data_group, "WeightedSigma")
        self.__n_events = 0
        try:
            self.__n_events = data_group.attrs["nEvents"]
        except:
            self.__n_events = -1

    def __assign_attr(self, data_group, name):
        try:
            return [attr for attr in data_group.attrs[name]]
        except:
            logging.debug("Attribute %s not available" % (name))
            return []

    def __get_value(self, i, var, except_return=float('nan')):
        if i >= len(var):
            logging.warning("<SciKitDataSetMetaData>: The index %d is out of range" % (n))
            return except_return
        return var[i]

    def get_maximum(self, i):
        return self.__get_value(i, self.__maxima)

    def get_minimum(self, i):
        return self.__get_value(i, self.__minima)

    def get_varname(self, i):
        return self.__get_value(i, self.__varnames, except_return="")

    def get_varlabel(self, i):
        return self.__get_value(i, self.__labels, except_return="")

    def get_mean(self, i):
        return self.__get_value(i, self.__mean)

    def get_w_mean(self, i):
        return self.__get_value(i, self.__w_mean)

    def get_sigma(self, i):
        return self.__get_value(i, self.__sigma)

    def get_w_sigma(self, i):
        return self.__get_value(i, self.__w_sigma)

    def get_dimension(self):
        return len(self.__varnames)

    def get_nevents(self):
        return self.__n_events

    def get_chunk_size(self):
        return self.__chunk_size

    def get_max_deviation(self, i, n_sigma=4, use_weights=False):
        mean = self.get_w_mean(i) if use_weights else self.get_mean(i)
        sigma = self.get_w_sigma(i) if use_weights else self.get_sigma(i)
        return min(self.get_maximum(i), mean + n_sigma * sigma)

    def get_min_deviation(self, i, n_sigma=4, use_weights=False):
        mean = self.get_w_mean(i) if use_weights else self.get_mean(i)
        sigma = self.get_w_sigma(i) if use_weights else self.get_sigma(i)
        return max(self.get_minimum(i), mean - n_sigma * sigma)


class SciKitDataSet(object):
    def __del__(self):
        self.closeFile()

    def __init__(self, infile="", SwapTrainTest=False, begin=0, end=-1):
        logging.info("<SciKitDataSet>: Open HDF5 File %s " % (infile))
        if not os.path.isfile(infile):
            logging.error("<SciKitDataSet>: File does not exist")
            exit(1)
        self.__file_name = ResolvePath(infile)
        self.__infile = h5py.File(self.__file_name, "r")
        if SwapTrainTest:
            logging.info("<SciKitDataSet>:  Swap the labeling between 'Training' and 'Testing'")
        self.__TrainGroupName = "Training" if not SwapTrainTest else "Testing"
        self.__TestGroupName = "Testing" if not SwapTrainTest else "Training"

        try:
            self.__train_in_meta = SciKitDataSetMetaData(self.HDF5File()[self.__TrainGroupName]["Data"])
        except:
            logging.warning("<SciKitDataSet>: No testing data available")
            self.__train_in_meta = None
        try:
            self.__test_in_meta = SciKitDataSetMetaData(self.HDF5File()[self.__TestGroupName]["Data"])
        except:
            logging.warning("<SciKitDataSet>: No testing data available")
            self.__test_in_meta = None
        try:
            self.__train_target_meta = SciKitDataSetMetaData(self.HDF5File()[self.__TrainGroupName]["ClassID"])
        except:
            self.__train_target_meta = None
        try:
            self.__test_target_meta = SciKitDataSetMetaData(self.HDF5File()[self.__TestGroupName]["ClassID"])
        except:
            self.__test_target_meta = None
        try:
            self.__topology = self.HDF5File().attrs["TopologyLabel"]
        except:
            self.__topology = ""
        ### Very late feature encode in the dataset whether it's made for regression or not.
        ### The coding is done by setting the Classification (32) or Regression (64) bit
        ### C.f. (https://gitlab.cern.ch/atlas-mpp-xampp/XAMPPtmva/blob/master/XAMPPtmva/XAMPPtmva/FactoryUtils.h)
        ### Please consider to update any changes in FactoryUtils also here.
        try:
            self.__data_type = self.HDF5File().attrs["trainingType"]
        except:
            self.__data_type = 32

        self.__TrainSet = None
        self.__TrainClassID = None
        self.__TrainWeights = None
        #### Define additional booleans
        #### to prevent the element-wise comparison
        #### overload of the = operator of the np.array
        self.__hasTrainSet = False
        self.__hasTrainClassID = False
        self.__hasTrainWeights = False

        self.__TestingSet = None
        self.__TestingClassID = None
        self.__TestingWeights = None

        self.__hasTestingSet = False
        self.__hasTestingClassID = False
        self.__hasTestingWeights = False

        self.__begin = begin
        self.__end = end
        self.__b_m_e = self.__end - self.__begin

    def file_name(self):
        return self.__file_name

    def isClassificationDS(self):
        return self.__data_type == 32

    def isRegressionDS(self):
        return self.__data_type == 64

    def HDF5File(self):
        return self.__infile

    def topology_label(self):
        return self.__topology

    def nVars(self):
        return self.__train_in_meta.get_dimension()

    def chunkSize(self):
        return self.__train_in_meta.get_chunk_size()

    def nTargets(self):
        return self.__train_target_meta.get_dimension()

    def nTrainingEvents(self):
        n = self.__train_in_meta.get_nevents() - self.__begin
        if self.__b_m_e > 0: return min([n, self.__b_m_e])
        return n

    def nTestingEvents(self):
        n = self.__test_in_meta.get_nevents() - self.__begin
        if self.__b_m_e > 0: return min([n, self.__b_m_e])
        return n

    def variableName(self, n):
        return self.__train_in_meta.get_varname(n)

    def variableLabel(self, n):
        return self.__train_in_meta.get_varlabel(n)

    def targetVariable(self, n):
        return self.__train_target_meta.get_varname(n)

    def LoadNPfromH5(self, group=None, dataset=None):
        logging.info("<SciKitDataSet>: Load %s from %s" % (dataset, group))

        H5File = self.HDF5File()
        data_path = "%s/%s" % (group, dataset) if group else dataset
        if not dataset or data_path not in H5File:
            logging.error("<SciKitDataSet>: Could not load dataset %s from H5 file" % (dataset))
            return np.array([0])
        return H5File[data_path]
        #### Use the HDF5 matrix object from Keras since it allows for shuffling of the internal state
        #### and can be directly piped to keras without
        #### Set the keras backend to theano before loading any keras
        #### library
        try:
            from keras.utils.io_utils import HDF5Matrix
        except:
            from XAMPPtmva.LoadHDF5 import set_keras_backend
            set_keras_backend()
            from keras.utils.io_utils import HDF5Matrix
        H5_mat = HDF5Matrix(
            datapath=H5File.filename,
            dataset=data_path,
            start=0,
            end=None,
        )
        return H5_mat

    def LoadData(self, isTesting):
        return self.LoadNPfromH5(group=self.__TrainGroupName if not isTesting else self.__TestGroupName, dataset="Data")

    def LoadClassifier(self, isTesting):
        return self.LoadNPfromH5(group=self.__TrainGroupName if not isTesting else self.__TestGroupName, dataset="ClassID")

    def LoadWeights(self, isTesting):
        return self.LoadNPfromH5(group=self.__TrainGroupName if not isTesting else self.__TestGroupName, dataset="Weights")

    def closeFile(self):
        if self.__infile != None: self.__infile.close()
        self.__infile = None

        self.__TrainSet = None
        self.__TrainClassID = None
        self.__TrainWeights = None
        #### Define additional booleans
        #### to prevent the element-wise comparison
        #### overload of the = operator of the np.array
        self.__hasTrainSet = False
        self.__hasTrainClassID = False
        self.__hasTrainWeights = False

        self.__TestingSet = None
        self.__TestingClassID = None
        self.__TestingWeights = None

        self.__hasTestingSet = False
        self.__hasTestingClassID = False
        self.__hasTestingWeights = False

    ###########################################
    ###########################################
    ###     Retrieve the Training data      ###
    ###########################################
    ###########################################
    def begin(self):
        return self.__begin

    def end(self):
        return self.__end

    def getTestingMax(self, n):
        return self.__test_in_meta.get_maximum(n)

    def getTestingMin(self, n):
        return self.__test_in_meta.get_minimum(n)

    def getTrainingMax(self, n):
        return self.__train_in_meta.get_maximum(n)

    def getTrainingMin(self, n):
        return self.__train_in_meta.get_minimum(n)

    def getTargetMax(self, n):
        return max(self.__train_target_meta.get_maximum(n), self.__test_target_meta.get_maximum(n))

    def getTargetMin(self, n):
        return min(self.__train_target_meta.get_minimum(n), self.__test_target_meta.get_minimum(n))

    def getMaxTrainDeviation(self, n, n_sigma, use_weights=True):
        return self.__train_in_meta.get_max_deviation(n, n_sigma=n_sigma, use_weights=use_weights)

    def getMaxTestDeviation(self, n, n_sigma, use_weights=True):
        return self.__test_in_meta.get_max_deviation(n, n_sigma=n_sigma, use_weights=use_weights)

    def getMaxTargetTrainDeviation(self, n, n_sigma, use_weights=True):
        return self.__train_target_meta.get_max_deviation(n, n_sigma=n_sigma, use_weights=use_weights)

    def getMaxTargetTestDeviation(self, n, n_sigma, use_weights=True):
        return self.__test_target_meta.get_max_deviation(n, n_sigma=n_sigma, use_weights=use_weights)

    def getMinTrainDeviation(self, n, n_sigma, use_weights=True):
        return self.__train_in_meta.get_min_deviation(n, n_sigma=n_sigma, use_weights=use_weights)

    def getMinTestDeviation(self, n, n_sigma, use_weights=True):
        return self.__test_in_meta.get_min_deviation(n, n_sigma=n_sigma, use_weights=use_weights)

    def getMinTargetTrainDeviation(self, n, n_sigma, use_weights=True):
        return self.__train_target_meta.get_min_deviation(n, n_sigma=n_sigma, use_weights=use_weights)

    def getMinTargetTestDeviation(self, n, n_sigma, use_weights=True):
        return self.__test_target_meta.get_min_deviation(n, n_sigma=n_sigma, use_weights=use_weights)

    def getTrainingData(self, begin=0, num=-1):
        if not self.__hasTrainSet:
            self.__TrainSet = self.LoadData(isTesting=False)
            self.__hasTrainSet = True
        if begin == 0 and num < 0: return self.__TrainSet
        return self.__TrainSet[begin:begin + min((num if num > 0 else self.nTrainingEvents()), self.nTrainingEvents() - begin)]

    def getTrainingWeights(self, begin=0, num=-1):
        if not self.__hasTrainWeights:
            self.__TrainWeights = self.LoadWeights(isTesting=False)
            self.__hasTrainWeights = True
        if begin == 0 and num == -1: return self.__TrainWeights
        return self.__TrainWeights[begin:begin + min((num if num > 0 else self.nTrainingEvents()), self.nTrainingEvents() - begin)]

    def getTrainingClassifier(self, begin=0, num=-1):
        if not self.__hasTrainClassID:
            self.__TrainClassID = self.LoadClassifier(isTesting=False)
            self.__hasTrainClassID = True
        if begin == 0 and num == -1: return self.__TrainClassID
        return self.__TrainClassID[begin:begin + min((num if num > 0 else self.nTrainingEvents()), self.nTrainingEvents() - begin)]

    ###########################################
    ###########################################
    ###      Retrieve the Testing data      ###
    ###########################################
    ###########################################
    def getTestingData(self, begin=0, num=-1):
        if not self.__hasTestingSet:
            self.__TestingSet = self.LoadData(isTesting=True)
            self.__hasTestingSet = True
        if begin == 0 and num == -1:
            return self.__TestingSet
        return self.__TestingSet[begin:begin + min((num if num > 0 else self.nTestingEvents()), self.nTestingEvents() - begin)]

    def getTestingWeights(self, begin=0, num=-1):
        if not self.__hasTestingWeights:
            self.__TestingWeights = self.LoadWeights(isTesting=True)
            self.__hasTestingWeights = True
        if begin == 0 and num == -1: return self.__TestingWeights
        return self.__TestingWeights[begin:begin + min((num if num > 0 else self.nTestingEvents()), self.nTestingEvents() - begin)]

    def getTestingClassifier(self, begin=0, num=-1):
        if not self.__hasTestingClassID:
            self.__TestingClassID = self.LoadClassifier(isTesting=True)
            self.__hasTestingClassID = True
        if begin == 0 and num == -1: return self.__TestingClassID
        return self.__TestingClassID[begin:begin + min((num if num > 0 else self.nTestingEvents()), self.nTestingEvents() - begin)]


#### The SciKitDataLoader
class SciKitDataLoader(threading.Thread):
    def __init__(self,
                 dataset=None,
                 process_testing=False,
                 process_signal=False,
                 process_background=False,
                 useWeights=True,
                 begin=0,
                 end=-1,
                 progress=True,
                 chunksize=1000):
        ### Constructor of the threading class
        threading.Thread.__init__(self)

        ### SciKitdataset object which created the class instance
        self.__dataset = dataset
        ### Run options
        self.__doTesting = process_testing
        self.__doSig = process_signal
        self.__doBkg = process_background
        self.__useWeights = useWeights

        self.__begin = begin
        self.__end = end if end < self.getNumEvents(False) else self.getNumEvents(False)
        self.__chunkSize = chunksize

        self.__prompt = int(self.getNumEvents() / 200) if progress and self.getNumEvents() > 1000 else -1
        self.__lastPrompted = self.__prompt
        self.__isOk = False

        ### Variables to cache the data from the HDF5
        self.__IDchunk = -1
        self.__IDdata = None
        self.__EvtToSelect = None

        self.__WeightChunk = -1
        self.__WeightData = None

        self.__DataChunk = -1
        self.__Data = None

    def chunkSize(self):
        return self.__chunkSize

    #############################################################
    #   The fillChunk method is intended to be overwritten by   #
    #   each inheriting class to perform the calculation        #
    #   for each event.                                         #
    #   Inheriting class obtain needed information via          #
    #                                                           #
    #   input data:            self.current_dataBlock()         #
    #   corresponding weights: self.current_weightBlock()       #
    #   classfier outcome:     self.current_classIDBlock()      #
    #############################################################
    def fillChunk(self):
        return True

    #####################################################################
    #   Finish is called after the loop is done. There you can finalize #
    #   the calculations.                                               #
    #####################################################################
    def finish(self):
        return

    # Returns whether everything went okay or not during the loop
    def isGoodState(self):
        return self.__isOk

    def dataset(self):
        return self.__dataset

    def useWeights(self):
        return self.__useWeights

    def doTestingEvts(self):
        return self.__doTesting

    def doBackground(self):
        return self.__doBkg

    def doSignal(self):
        return self.__doSig

    #-------------------------------------------------------
    ###        Methods to interact with the  HDF5 file
    #-------------------------------------------------------
    def __getStartEvent(self, chunk):
        start = chunk * self.chunkSize()
        if self.getNumEvents() <= start: return -1
        return start

    def getNumEvents(self, consid_end=True):
        if consid_end and self.__end > 0: return self.__end
        if self.__dataset == None: return 0
        if self.__doTesting: return self.__dataset.nTestingEvents()
        return self.__dataset.nTrainingEvents()

    def __loadClassID(self, chunk=0):
        if self.__IDchunk == chunk: return True
        chunk_to_load = self.__getStartEvent(chunk)
        self.__IDdata = None
        if chunk_to_load == -1: return False
        self.__IDchunk = chunk

        if self.__doTesting: self.__IDdata = self.__dataset.getTestingClassifier(begin=chunk_to_load, num=self.chunkSize())
        else: self.__IDdata = self.__dataset.getTrainingClassifier(begin=chunk_to_load, num=self.chunkSize())

        bkg = 0 if self.__doBkg else -1
        sig = 1 if self.__doSig else -1

        if self.__dataset.isClassificationDS(): self.__EvtToSelect = np.logical_or(self.__IDdata == bkg, self.__IDdata == sig)
        else: self.__EvtToSelect = np.ones(len(self.__IDdata), dtype=bool)
        return True

    def __loadWeight(self, chunk=0):
        if self.__WeightChunk == chunk: return True
        self.__WeightData = None
        chunk_to_load = self.__getStartEvent(chunk)
        if chunk_to_load == -1: return False
        self.__WeightChunk = chunk
        if self.__useWeights:
            if self.__doTesting:
                self.__WeightData = self.__dataset.getTestingWeights(begin=chunk_to_load, num=self.chunkSize())[self.__EvtToSelect]
            else:
                self.__WeightData = self.__dataset.getTrainingWeights(begin=chunk_to_load, num=self.chunkSize())[self.__EvtToSelect]
        else:
            self.__WeightData = np.ones(len(self.__EvtToSelect[self.__EvtToSelect]))

        return True

    def __loadData(self, chunk=0):
        if self.__DataChunk == chunk: return True
        self.__Data = None
        chunk_to_load = self.__getStartEvent(chunk)
        if chunk_to_load == -1: return False
        self.__DataChunk = chunk
        if self.__doTesting:
            self.__Data = self.__dataset.getTestingData(begin=chunk_to_load, num=self.chunkSize())[self.__EvtToSelect]
        else:
            self.__Data = self.__dataset.getTrainingData(begin=chunk_to_load, num=self.chunkSize())[self.__EvtToSelect]
        return True

    def current_dataBlock(self):
        return self.__Data

    def current_weightBlock(self):
        return self.__WeightData

    def current_classIDBlock(self):
        return self.__IDdata

    def run(self):
        if self.__dataset == None: return
        ### Is there a limit on the number of events?
        chunk = 0 if self.__begin < self.chunkSize() else (self.__begin - self.__begin % self.chunkSize()) / self.chunkSize()

        time_start = time.clock()
        current_ev = 0
        while (self.__loadClassID(chunk)):
            if not self.__loadData(chunk) or not self.__loadWeight(chunk): return False
            chunk += 1
            if len(self.__Data) == 0:
                current_ev += self.chunkSize()
                continue
            if not self.fillChunk(): return False
            else:
                current_ev += len(self.__Data)
                ### Prompt the events
                if self.__prompt <= 0: continue
                if current_ev > 0 and current_ev > self.__lastPrompted:
                    self.__lastPrompted += self.__prompt
                    progress = 1. * current_ev / self.getNumEvents()
                    elapsed_time = time.clock() - time_start
                    rate = current_ev / max([(time.clock() - time_start), 1])
                    logging.info("<SciKitDataLoader>: Processed %d/%d (%.1f %%) events after %s. Current event rate = %.1f kHz. E.T.A: %s" %
                                 (current_ev, self.getNumEvents(), progress * 100, time.strftime("%H:%M:%S", time.gmtime(elapsed_time)),
                                  rate / 1000., time.strftime("%H:%M:%S", time.gmtime((self.getNumEvents() - current_ev) / rate))))
        self.__isOk = True
        self.finish()
        self.__dataset = None
